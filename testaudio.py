#!/usr/bin/python3
# -*- coding: utf-8 -*-

import simpleaudio as sa

def wave_play(trek):
    wave_obj = sa.WaveObject.from_wave_file(trek)
    play_obj = wave_obj.play()
    play_obj.wait_done()

wave_play("c.wav")
