#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import requests

IAM_TOKEN = "CggVAgAAA..."
cloud_id = "b1g3vn..."
ID_FOLDER= "b1ghig..."
URL_REC = "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?"

with open("./2.ogg", "rb") as f:
    data = f.read()


def recognize(name_guest, URL_REC, IAM_TOKEN, ID_FOLDER):
    # в поле заголовка передаем IAM_TOKEN:
    headers = {'Authorization': f'Bearer {IAM_TOKEN}'}
    # остальные параметры:
    params = "&".join([
        "topic=general",
        f"folderId={ID_FOLDER}",
        "lang=ru-RU"
        ])
    response = requests.post(URL_REC, params=params, headers=headers, data=data)
    # бинарные ответ доступен через response.content, декодируем его:
    decode_resp = response.content.decode('UTF-8')
    # и загрузим в json, чтобы получить текст из аудио:
    result = json.loads(decode_resp)
    text = result['result']

    return text

name = recognize(data, URL_REC, IAM_TOKEN, ID_FOLDER)

print(f"Результат: {name}")
